package com.example.android.flixmovie.Presentation;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.Toast;

import com.example.android.flixmovie.Domain.ListAdapter;
import com.example.android.flixmovie.Domain.Movie;
import com.example.android.flixmovie.R;

import java.util.ArrayList;

public class MyListsActivity extends AppCompatActivity implements ListAdapter.itemClickListener {

    private final ArrayList<Movie> movieList = new ArrayList<>();
    private RecyclerView mRecyclerView;
    private ListAdapter listAdapter;

    public void addToArraylist(Movie movie){
        this.movieList.add(movie);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.flixmovie_listrecyclerview);

        mRecyclerView = (RecyclerView) findViewById(R.id.rv_list_flixmovie);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(this));

        listAdapter = new ListAdapter(this);
        mRecyclerView.setAdapter(listAdapter);
    }

    @Override
    public void onListItemClick(int clickPosition) {
        Context context = MyListsActivity.this;
        Class destinationClass = MovieDetailActivity.class;
        Toast mDetailToast = Toast.makeText(this, "Movie details loaded", Toast.LENGTH_SHORT);

        Intent startMovieDetailActivity = new Intent(context, destinationClass);

        Movie m = movieList.get(clickPosition);

        startMovieDetailActivity.putExtra("Title", m.getTitle());
        startMovieDetailActivity.putExtra("Genre", m.getGenre());
        startMovieDetailActivity.putExtra("Runtime", m.getRunTime());
        startMovieDetailActivity.putExtra("Age", m.getAge());
        startMovieDetailActivity.putExtra("Rating", m.getMovieRating());
        startMovieDetailActivity.putExtra("Language", m.getLanguage());
        startMovieDetailActivity.putExtra("ReleaseDate", m.getDateOfRelease());
        startMovieDetailActivity.putExtra("Description", m.getDescription());
        startMovieDetailActivity.putExtra("Image", m.getImgURL());

        startActivity(startMovieDetailActivity);
        mDetailToast.show();
    }
}
