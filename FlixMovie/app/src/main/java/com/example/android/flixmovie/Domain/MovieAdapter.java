package com.example.android.flixmovie.Domain;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.android.flixmovie.Presentation.MyListsActivity;
import com.example.android.flixmovie.R;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class MovieAdapter extends RecyclerView.Adapter<MovieAdapter.MovieViewHolder> {

    // Log for debug
    private final String TAG = MovieAdapter.class.getSimpleName();

    // Creating variables
    private ArrayList<Movie> movieList;
    final itemClickListener mOnClickListener;

    // Adapter constructor
    public MovieAdapter(ArrayList<Movie> movieList, itemClickListener listener) {
        this.movieList = movieList;
        this.mOnClickListener = listener;
    }

    @NonNull
    @Override
    public MovieAdapter.MovieViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {

        Context context = viewGroup.getContext();
        LayoutInflater inflater = LayoutInflater.from(context);
        View listRow = inflater.inflate(R.layout.flixmovie_listview, viewGroup, false);

        MovieViewHolder movieViewHolder = new MovieViewHolder(listRow);

        return movieViewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull MovieAdapter.MovieViewHolder movieViewHolder, int position) {
        Movie movie = movieList.get(position);

        movieViewHolder.tv_movieTitle.setText(movie.getTitle());
        Picasso.get().load(movie.getImgURL()).into(movieViewHolder.iv_imgview);

        String genreString = "";
        for (String genre: movie.getGenre()) {

            if (movie.getGenre().size() == 1) {
                genreString += genre;
            } else {
                genreString += genre + " | ";
            }
        }

        movieViewHolder.tv_genre.setText(genreString);

//        movieViewHolder.tv_durationTime.setText(movie.getDurationTime() + "min");
        movieViewHolder.tv_movieRating.setText("Rating: " + movie.getMovieRating());
        if (movie.getMovieRating() == 0.0) {
            movieViewHolder.tv_movieRating.setText("Rating: -");
        }

    }

    // Counts all items in array list
    @Override
    public int getItemCount() {
        return movieList.size();
    }

    // Creating list item clicker
    public interface itemClickListener {
        void onListItemClick(int clickPosition);
    }

    // Create MovieViewHolder with imageview and textview in the recyclerview with clicklisteners
    public class MovieViewHolder extends RecyclerView.ViewHolder
            implements View.OnClickListener {

        private ImageView iv_imgview;
        private TextView tv_movieTitle;
        private TextView tv_genre;
        private TextView tv_movieRating;
        private ImageButton btn_listAdd;
        private MyListsActivity myListsActivity = new MyListsActivity();


        public MovieViewHolder(@NonNull final View itemView) {
            super(itemView);

            iv_imgview = itemView.findViewById(R.id.iv_imgview);
            tv_movieTitle = itemView.findViewById(R.id.tv_movieTitle);
            tv_genre = itemView.findViewById(R.id.tv_genre);
            tv_movieRating = itemView.findViewById(R.id.tv_movieRating);
            btn_listAdd = itemView.findViewById(R.id.btn_addToList);

            tv_movieTitle.setOnClickListener(this);
            iv_imgview.setOnClickListener(this);
            tv_genre.setOnClickListener(this);
            tv_movieRating.setOnClickListener(this);

            btn_listAdd.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Movie movie = movieList.get(getAdapterPosition());
                    myListsActivity.addToArraylist(movie);
                }
            });
        }

        @Override
        public void onClick(View view) {
            int clickPosition = getAdapterPosition();
            mOnClickListener.onListItemClick(clickPosition);
        }
    }
}
