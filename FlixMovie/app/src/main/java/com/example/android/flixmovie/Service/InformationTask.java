package com.example.android.flixmovie.Service;

import android.net.sip.SipSession;
import android.os.AsyncTask;
import android.util.Log;

import com.example.android.flixmovie.Domain.Movie;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Array;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.Scanner;

public class InformationTask extends AsyncTask<Void, Void, String> {

    // Log variable
    private final String TAG = InformationTask.class.getSimpleName();

    // Creating all variables, getting the api from website
    private String imageGetter = "https://image.tmdb.org/t/p/w500";

    private Listener listener;
    private String mApiUrl;
    private String prefferedGenre;
    private int page = 1;

    // The constructor of the InformationTask
    public InformationTask(Listener listener, String mApiUrl, String prefferedGenre) {
        this.listener = listener;
        this.mApiUrl = mApiUrl;
        this.prefferedGenre = prefferedGenre;
    }

    public InformationTask(Listener listener, String mApiUrl) {
        this.listener = listener;
        this.mApiUrl = mApiUrl;
        this.prefferedGenre = "";
    }

    @Override
    protected String doInBackground(Void... voids) {

        String response = "";
        try {
            URL mUrl = new URL(this.mApiUrl);
            URLConnection urlConnection = mUrl.openConnection();
            HttpURLConnection httpURLConnection = (HttpURLConnection) urlConnection;
            httpURLConnection.setRequestMethod("GET");

            httpURLConnection.connect();

            int responseCode = httpURLConnection.getResponseCode();

            if (responseCode == 200) {
                InputStream in = httpURLConnection.getInputStream();

                Scanner scanner = new Scanner(in);
                scanner.useDelimiter("\\A");

                boolean hasInput = scanner.hasNext();

                if (hasInput == true) {
                    response = scanner.next();
                } else {
                    Log.e(TAG, "Error: " + responseCode + " , " + response);
                }
            }
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        Log.d(TAG, "RESPONSE: " + response);
        return response;
    }

    @Override
    protected void onPostExecute(String response) {
        super.onPostExecute(response);

        ArrayList<Movie> movieList = new ArrayList<>();

        try {
            JSONObject results = new JSONObject(response);
            Log.d(TAG, "RESULTS = " + results);

            JSONArray resultsfullmovielist = results.getJSONArray("results");

            for (int i = 0; i < resultsfullmovielist.length(); i++) {
                JSONObject movie = resultsfullmovielist.getJSONObject(i);

                int movieID = movie.getInt("id");
                String title = movie.getString("title");

                JSONArray genreIDS = movie.getJSONArray("genre_ids");

                ArrayList<String> genreIDSArray = new ArrayList<>();

                for (int y = 0; y < genreIDS.length(); y++) {
                    int genreid = genreIDS.getInt(y);
                    String genrename = genreConverter(genreid);
                    genreIDSArray.add(genrename);
                }

                String imageinfo = imageGetter + movie.getString("poster_path");

                String movieDescription = movie.getString("overview");
                String movieLanguage = movie.getString("original_language");
                String movieReleaseDate = movie.getString("release_date");
                double movieRating = movie.getDouble("vote_average");
                String movieAge = movie.getString("adult");

//                int movieRunTime = movie.getInt("runtime");

                Movie addmovie = new Movie(movieID, title, genreIDSArray, imageinfo, movieDescription, movieLanguage, movieReleaseDate, movieRating, movieAge);

                if (prefferedGenre == "") {
                    movieList.add(addmovie);
                } else {
                    for (String genre : addmovie.getGenre()) {
                        if (genre.equals(prefferedGenre)) {
                            movieList.add(addmovie);
                        }
                    }
                }
            }
                listener.onMovieAvailable(movieList);
            } catch(JSONException e){
                e.printStackTrace();
            }
        }


    public String genreConverter(int integerGenre){
        ArrayList<String> resultsGenres = new ArrayList<>();

            if (integerGenre == 28){
                resultsGenres.add("Action");
            } else if (integerGenre == 12){
                resultsGenres.add("Adventure");
            } else if (integerGenre == 16){
                resultsGenres.add("Animation");
            }else if (integerGenre == 35){
                resultsGenres.add("Comedy");
            }else if (integerGenre == 80){
                resultsGenres.add("Crime");
            }else if (integerGenre == 99){
                resultsGenres.add("Documentary");
            }else if (integerGenre == 18){
                resultsGenres.add("Drama");
            }else if (integerGenre == 10751){
                resultsGenres.add("Family");
            }else if (integerGenre == 14){
                resultsGenres.add("Fantasy");
            }else if (integerGenre == 36){
                resultsGenres.add("History");
            }else if (integerGenre == 27){
                resultsGenres.add("Horror");
            }else if (integerGenre == 10402){
                resultsGenres.add("Music");
            }else if (integerGenre == 9648){
                resultsGenres.add("Mystery");
            }else if (integerGenre == 10749){
                resultsGenres.add("Romance");
            }else if (integerGenre == 878){
                resultsGenres.add("Science Fiction");
            }else if (integerGenre == 10770){
                resultsGenres.add("TV Movie");
            }else if (integerGenre == 53){
                resultsGenres.add("Thriller");
            }else if (integerGenre == 10752){
                resultsGenres.add("War");
            }else if (integerGenre == 37){
                resultsGenres.add("Western");
            }
        return resultsGenres.get(0);
    }

    // Gives new information or movie tot the onMovieAvailable interface
    public interface Listener {
        void onMovieAvailable(ArrayList<Movie> movieList);
    }
}
