package com.example.android.flixmovie.Presentation;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ShareActionProvider;
import android.widget.TextView;
import android.widget.Toast;

import com.example.android.flixmovie.Domain.List;
import com.example.android.flixmovie.Domain.Movie;
import com.example.android.flixmovie.R;
import com.example.android.flixmovie.Service.InformationTask;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class MovieDetailActivity extends AppCompatActivity {

    private final String TAG = MovieDetailActivity.class.getSimpleName();

    private ImageView mImageView;
    private TextView mMovieTitle;
    private TextView mGenre;
    private TextView mRunTime;
    private TextView mMovieRating;
    private TextView mAge;
    private TextView mLanguage;
    private TextView mReleaseDate;
    private TextView mMovieDescription;
    private Button mTrailerButton;
    private String movieTitle;

    String description;
    String language;
    String releaseDate;
    double movieRating;
    String age;
    String trailerURL = "https://www.youtube.com/results?search_query=";
    String trailerURLforMessage;

    private String imgUrl = "";
    private ArrayList<String> movieGenre = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.flixmovie_detailview);

        mImageView = (ImageView) findViewById(R.id.iv_imgview);
        mMovieTitle = (TextView) findViewById(R.id.tv_movieTitle);
        mGenre = (TextView) findViewById(R.id.tv_genre);
        mRunTime = (TextView) findViewById(R.id.tv_runTime);
        mMovieRating = (TextView) findViewById(R.id.tv_movieRating);
        mAge = (TextView) findViewById(R.id.tv_age);
        mLanguage = (TextView) findViewById(R.id.tv_language);
        mReleaseDate = (TextView) findViewById(R.id.tv_releaseDate);
        mMovieDescription = (TextView) findViewById(R.id.tv_movieDescription);
        mTrailerButton = (Button) findViewById(R.id.btn_trailer);

        onImageClick();
        movieHandler();

        mTrailerButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                trailerURL = "https://www.youtube.com/results?search_query=";
                String trailerURLEnd = apihandler();

                Intent browserIntent = new Intent(
                        Intent.ACTION_VIEW,
                        Uri.parse(trailerURL + trailerURLEnd));
                trailerURLforMessage = trailerURL + trailerURLEnd;
                startActivity(browserIntent);
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu){
        getMenuInflater().inflate(R.menu.detailmenu, menu);
        MenuItem share = menu.findItem(R.id.btn_shareMovie);

        share.setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {

            @Override
            public boolean onMenuItemClick(MenuItem item) {
                Intent sendIntent = new Intent();
                sendIntent.setAction(Intent.ACTION_SEND);

                String trailerURLEnd = apihandler();

                sendIntent.putExtra(Intent.EXTRA_TEXT, "Have you ever seen: " + movieTitle + "\n .Its about: " + description + "\n you can watch the trailer at: " + trailerURL + trailerURLEnd);
                sendIntent.setType("text/plain");
                startActivity(sendIntent);

                return false;
            }
        });
        return true;
    }

    public void movieHandler() {
        Intent detailIntent = getIntent();

        if (detailIntent.hasExtra("Title")) {
            this.movieTitle = detailIntent.getStringExtra("Title");
            mMovieTitle.setText(movieTitle);
        } else {
            mMovieTitle.setText("Title Unknown!");
        }

        if (detailIntent.hasExtra("Genre")) {
            movieGenre = detailIntent.getStringArrayListExtra("Genre");
            for (int i = 0; i < movieGenre.size(); i++){

                mGenre.setText(mGenre.getText() + movieGenre.get(i) + " | ");
            }
        } else {
            mGenre.setText("Genre Unknown!");
        }

        if (detailIntent.hasExtra("Runtime")) {
            String runTime = detailIntent.getStringExtra("Runtime");
            mRunTime.setText("Runtime: " + runTime);

        } else {
             mRunTime.setText("Runtime Unknown!");
        }

        if (detailIntent.hasExtra("Age")) {
            age = detailIntent.getStringExtra("Age");
            if (age.equalsIgnoreCase("true"))
            mRunTime.setText("Age: " + "18+");
            else if (age.equalsIgnoreCase("false")){
                mRunTime.setText("Age: " + "< 16");
            }
        } else {
            mRunTime.setText("Age Unknown!");
        }

        if (detailIntent.hasExtra("Rating")) {
            movieRating = detailIntent.getDoubleExtra("Rating", 0.0);
            mMovieRating.setText("Rating: " + movieRating);
        } else {
            mMovieRating.setText("Rating Unknown!");
        }

        if (detailIntent.hasExtra("Language")) {
            language = detailIntent.getStringExtra("Language");
            mLanguage.setText(language);
        } else {
            mLanguage.setText("Language Unknown!");
        }

        if (detailIntent.hasExtra("ReleaseDate")) {
            releaseDate = detailIntent.getStringExtra("ReleaseDate");
            mReleaseDate.setText(releaseDate);
        } else {
            mReleaseDate.setText("Release Date Unknown!");
        }

        if (detailIntent.hasExtra("Description")) {
            description = detailIntent.getStringExtra("Description");
            mMovieDescription.setText(description);
        } else {
            mMovieDescription.setText("Description Unknown!");
        }

        if (detailIntent.hasExtra("Image")) {
            String image = detailIntent.getStringExtra("Image");
            Picasso.get().load(image).into(mImageView);

            imgUrl+= image;
        }
    }

    public String apihandler(){
        String apiMiddle = "" ;

        for(int i = 0; i < movieTitle.length(); i++){
            if (movieTitle.charAt(i) == ' '){
                apiMiddle += "+";
            } else {
                apiMiddle += movieTitle.charAt(i);
            }
        }
        return apiMiddle + " trailer";
    }

    public void onImageClick(){
        mImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Context context = MovieDetailActivity.this;
                Class destinationClass = FullScreenImgActivity.class;

                Intent startFullScreenImgActivity = new Intent(context, destinationClass);
                startFullScreenImgActivity.putExtra("Image", imgUrl);

                startActivity(startFullScreenImgActivity);
            }
        });
    }
}
