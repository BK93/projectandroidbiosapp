package com.example.android.flixmovie.Domain;

import java.util.ArrayList;

public class List {

    final ArrayList<Movie> movieArrayList = new ArrayList<>();

    private List() {
    }

    static List getInstance() {
        if (instance == null) {
            instance = new List();
        }
        return instance;
    }

    private static List instance;

}
