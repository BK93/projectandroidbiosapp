package com.example.android.flixmovie.Presentation;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.SearchView;
import android.widget.Toast;

import com.example.android.flixmovie.Domain.Movie;
import com.example.android.flixmovie.Domain.MovieAdapter;
import com.example.android.flixmovie.R;
import com.example.android.flixmovie.Service.InformationTask;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity implements InformationTask.Listener, MovieAdapter.itemClickListener {

    private final String TAG = MainActivity.class.getSimpleName();

    private ArrayList<Movie> movieList = new ArrayList<>();
    private RecyclerView mRecyclerView;
    private MovieAdapter movieAdapter;
    private InformationTask informationTask;
    private String prefferedGenre = "";
//    private ImageButton mAddMovie;

    //different API url's for the lists
    private String popularMoviesURL = "https://api.themoviedb.org/3/movie/popular?api_key=00ed7a6268fb89848c2f989c4b8eac65&language=en-US&page=1";
    private String topratedMoviesURL = "https://api.themoviedb.org/3/movie/top_rated?api_key=00ed7a6268fb89848c2f989c4b8eac65&language=en-US&page=1";
    private String upcomingMoviesURL = "https://api.themoviedb.org/3/movie/upcoming?api_key=00ed7a6268fb89848c2f989c4b8eac65&language=en-US&page=1";
    private String nowplayingMoviesURL = "https://api.themoviedb.org/3/movie/now_playing?api_key=00ed7a6268fb89848c2f989c4b8eac65&language=en-US&page=1";

    private String standardAPI = "https://api.themoviedb.org/3/movie/popular?api_key=00ed7a6268fb89848c2f989c4b8eac65&language=en-US&page=1";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.flixmovie_recyclerview);

        mRecyclerView = (RecyclerView) findViewById(R.id.rv_flixmovie_list);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(this));

        movieAdapter = new MovieAdapter(movieList, this);
        mRecyclerView.setAdapter(movieAdapter);
//        mAddMovie = (ImageButton) findViewById(R.id.btn_addToList);

        // Creating the InformationTask to get information from json code
        informationTask = new InformationTask(this, this.popularMoviesURL);
        informationTask.execute();

        Toast toast = Toast.makeText(this, "Movies Loaded!", Toast.LENGTH_LONG);
        toast.show();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu, menu);
        MenuItem search = menu.findItem(R.id.searchButton);
        SearchView searchView = (SearchView) search.getActionView();

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {

            String api = "https://api.themoviedb.org/3/search/movie?api_key=00ed7a6268fb89848c2f989c4b8eac65&language=en-US&query=";
            String apiEnd = "&page=1&include_adult=false";


            @Override
            public boolean onQueryTextSubmit(String query) {
                String apiMiddle = "" ;

                if (query.equals("Horror")){
                    genreHandler("Horror");
                } else if (query.equals("Music")){
                    genreHandler("Music");
                } else if (query.equals("Adventure")){
                    genreHandler("Adventure");
                } else if (query.equals("Animation")){
                    genreHandler("Animation");
                } else if (query.equals("Comedy")){
                    genreHandler("Comedy");
                } else if (query.equals("Crime")){
                    genreHandler("Crime");
                } else if (query.equals("Documentary")){
                    genreHandler("Documentary");
                } else if (query.equals("Drama")){
                    genreHandler("Drama");
                } else if (query.equals("Family")){
                    genreHandler("Family");
                } else if (query.equals("Fantasy")){
                    genreHandler("Fantasy");
                } else if (query.equals("History")){
                    genreHandler("History");
                } else if (query.equals("Mystery")){
                    genreHandler("Mystery");
                } else if (query.equals("Romance")){
                    genreHandler("Romance");
                } else if (query.equals("Science Fiction")){
                    genreHandler("Science Fiction");
                } else if (query.equals("TV Movie")){
                    genreHandler("TV Movie");
                } else if (query.equals("Thriller")){
                    genreHandler("Thriller");
                } else if (query.equals("War")){
                    genreHandler("War");
                } else if (query.equals("Western")){
                    genreHandler("Western");
                } else {
                    for(int i = 0; i < query.length(); i++){
                        if (query.charAt(i) == ' '){
                            apiMiddle += "%20";
                        } else {
                            apiMiddle += query.charAt(i);
                        }
                    }
                    getMovie(api + apiMiddle + apiEnd);
                }
                    return false;
                }

            @Override
            public boolean onQueryTextChange(String newText) {
                if (newText.isEmpty()){
                    getMovie(standardAPI);
                }
                return false;
            }
        });
        return true;
    }

    public void genreHandler(String prefferedGenre){
        InformationTask informationTask1 = new InformationTask(this, popularMoviesURL, prefferedGenre);
        informationTask1.execute();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case R.id.item1:
                InformationTask informationTask = new InformationTask(this, this.popularMoviesURL);
                standardAPI = this.popularMoviesURL;
                informationTask.execute();
                return true;
            case R.id.item2:
                InformationTask informationTask1 = new InformationTask(this, this.upcomingMoviesURL);
                standardAPI = this.upcomingMoviesURL;
                informationTask1.execute();
                return true;
            case R.id.item3:
                InformationTask informationTask2 = new InformationTask(this, this.topratedMoviesURL);
                standardAPI = this.topratedMoviesURL;
                informationTask2.execute();
                return true;
            case R.id.item4:
                InformationTask informationTask3 = new InformationTask(this, this.nowplayingMoviesURL);
                standardAPI = this.nowplayingMoviesURL;
                informationTask3.execute();
                return true;
            case R.id.item5:
                Context context = MainActivity.this;
                Class destinationClass = MyListsActivity.class;
                Intent startMyListsActivity = new Intent(context, destinationClass);
                startActivity(startMyListsActivity);
                return true;
         default: return super.onOptionsItemSelected(item);
        }
    }

    public void getMovie(String api){
        InformationTask informationTask1 = new InformationTask(this, api);
        informationTask1.execute();
    }

    // Check if there are new movies available, keeps list up to date
    @Override
    public void onMovieAvailable(ArrayList<Movie> movieList){
        this.movieList.clear();
        this.movieList.addAll(movieList);
        this.movieAdapter.notifyDataSetChanged();
    }

    @Override
    public void onListItemClick(int clickPosition) {
        Context context = MainActivity.this;
        Class destinationClass = MovieDetailActivity.class;
        Toast mDetailToast = Toast.makeText(this, "Movie details loaded", Toast.LENGTH_SHORT);

        Intent startMovieDetailActivity = new Intent(context, destinationClass);

        Movie m = movieList.get(clickPosition);

        startMovieDetailActivity.putExtra("Title", m.getTitle());
        startMovieDetailActivity.putExtra("Genre", m.getGenre());
        startMovieDetailActivity.putExtra("Runtime", m.getRunTime());
        startMovieDetailActivity.putExtra("Age", m.getAge());
        startMovieDetailActivity.putExtra("Rating", m.getMovieRating());
        startMovieDetailActivity.putExtra("Language", m.getLanguage());
        startMovieDetailActivity.putExtra("ReleaseDate", m.getDateOfRelease());
        startMovieDetailActivity.putExtra("Description", m.getDescription());
        startMovieDetailActivity.putExtra("Image", m.getImgURL());

        startActivity(startMovieDetailActivity);
        mDetailToast.show();
    }
}
