package com.example.android.flixmovie.Domain;

import java.util.ArrayList;
import java.util.Random;

public class Movie {

    private int movieId;
    private String title;
    private ArrayList<String> genre;
    private int runTime;
    private String language;
    private String description;
    private String imgURL;
    private String dateOfRelease;
    private double movieRating;
    private String age;


    public Movie(int movieId, String title, ArrayList<String> genre, String imgURL, String description, String language, String dateOfRelease, double movieRating, String age){
        this.movieId = movieId;
        this.title = title;
        this.genre = genre;
        this.runTime = setRunTime();
        this.imgURL = imgURL;
        this.description = description;
        this.language = language;
        this.dateOfRelease = dateOfRelease;
        this.movieRating = movieRating;
        this.age = age;
    }

    public Movie(String title, ArrayList<String> genre, String imgURL, String description, String language, String dateOfRelease, double movieRating, String age){
       this.title = title;
       this.genre = genre;
       this.imgURL = imgURL;
       this.description = description;
       this.language = language;
       this.dateOfRelease = dateOfRelease;
       this.movieRating = movieRating;
       this.age = age;
    }

    public Movie(String title){
        this.title = title;
    }

    public double getMovieRating(){return this.movieRating;}

    public String getLanguage() {
        return this.language;
    }

    public String getDescription() {
        return this.description;
    }

    public String getDateOfRelease() {
        return this.dateOfRelease;
    }

    public int getMovieId() {
        return this.movieId;
    }

    public String getTitle() {
        return this.title;
    }

    public ArrayList<String> getGenre() {
        return this.genre;
    }

    public String getImgURL() {
        return this.imgURL;
    }

    public String getAge() {
        return this.age;
    }

    public int getRunTime() {
        return this.runTime;
    }

    public int setRunTime(){
        Random r = new Random();
        int duration = r.nextInt(50);

        duration += 101;
        return duration;
    }
}
