package com.example.android.flixmovie.Domain;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.android.flixmovie.Presentation.MyListsActivity;
import com.example.android.flixmovie.R;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class ListAdapter extends RecyclerView.Adapter<ListAdapter.ListViewHolder> {

    public ArrayList<Movie> myMovies = List.getInstance().movieArrayList;
    final itemClickListener mOnClickListener;

    public ListAdapter(itemClickListener listener) {
        this.mOnClickListener = listener;
    }

    @NonNull
    @Override
    public ListAdapter.ListViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {

        Context context = viewGroup.getContext();
        LayoutInflater inflater = LayoutInflater.from(context);
        View myListRow = inflater.inflate(R.layout.flixmovie_mylistview, viewGroup, false);

        ListViewHolder listViewHolder = new ListViewHolder(myListRow);

        return listViewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull ListViewHolder listViewHolder, int i) {
        Movie movie = myMovies.get(i);

        listViewHolder.tv_movieTitle.setText(movie.getTitle());
        Picasso.get().load(movie.getImgURL()).into(listViewHolder.iv_imgview);

        String genreString = "";
        for (String genre: movie.getGenre()) {

            if (movie.getGenre().size() == 1) {
                genreString += genre;
            } else {
                genreString += genre + " | ";
            }
        }

        listViewHolder.tv_genre.setText(genreString);

//        listViewHolder.tv_durationTime.setText(movie.getDurationTime() + "min");
        listViewHolder.tv_movieRating.setText("Rating: " + movie.getMovieRating());
        if (movie.getMovieRating() == 0.0) {
            listViewHolder.tv_movieRating.setText("Rating: -");
        }
    }

    // Counts all items in array list
    @Override
    public int getItemCount() {
        return myMovies.size();
    }

    // Creating list item clicker
    public interface itemClickListener {
        void onListItemClick(int clickPosition);
    }

    public class ListViewHolder extends RecyclerView.ViewHolder
            implements View.OnClickListener {

        private ImageView iv_imgview;
        private TextView tv_movieTitle;
        private TextView tv_genre;
        private TextView tv_movieRating;


        public ListViewHolder(@NonNull final View itemView) {
            super(itemView);

            iv_imgview = itemView.findViewById(R.id.iv_imgview);
            tv_movieTitle = itemView.findViewById(R.id.tv_movieTitle);
            tv_genre = itemView.findViewById(R.id.tv_genre);
            tv_movieRating = itemView.findViewById(R.id.tv_movieRating);

            tv_movieTitle.setOnClickListener(this);
            iv_imgview.setOnClickListener(this);
            tv_genre.setOnClickListener(this);
            tv_movieRating.setOnClickListener(this);

        }

        @Override
        public void onClick(View view) {
            int clickPosition = getAdapterPosition();
            mOnClickListener.onListItemClick(clickPosition);
        }

    }
}
