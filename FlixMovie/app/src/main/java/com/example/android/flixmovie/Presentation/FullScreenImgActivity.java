package com.example.android.flixmovie.Presentation;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.ImageView;
import android.widget.Toast;

import com.example.android.flixmovie.R;
import com.squareup.picasso.Picasso;


public class FullScreenImgActivity extends AppCompatActivity {

    // Log for debug
    private final String TAG = FullScreenImgActivity.class.getSimpleName();

    // Creating all variabeles
    private ImageView mFullScreenImage;
    private Toast mToastImgInfo;
    private String imgUrl;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.flixmovie_full_screen_img);

        mFullScreenImage = (ImageView) findViewById(R.id.img_fullscreenMovie);

        fullScreenHandler();
    }

    public void fullScreenHandler(){
        Intent intent = getIntent();

        if (intent.hasExtra("Image")) {
            imgUrl = intent.getStringExtra("Image");

//            Log.d(TAG, imgUrl);

            Picasso.get().load(imgUrl).into(mFullScreenImage);
        }

        mToastImgInfo = Toast.makeText(this, "Poster Loaded!", Toast.LENGTH_SHORT);
        mToastImgInfo.show();
    }
}
