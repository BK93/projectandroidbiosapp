
-- Create FlixMovie database
USE master;
DROP DATABASE IF EXISTS FlixMovie;

CREATE DATABASE FlixMovie; 
USE FlixMovie;


DROP TABLE IF EXISTS List;
DROP TABLE IF EXISTS Movie;
DROP TABLE IF EXISTS Serie;

-- Create List table
CREATE TABLE List (
  ListId int NOT NULL,
  ListName varchar(255) NOT NULL,
  PRIMARY KEY (ListId)
);

-- Create Movie table
CREATE TABLE Movie (
  MovieId int NOT NULL,
  MovieName varchar(255) NOT NULL,
  Genre varchar(50) NOT NULL,
  RunTime int NOT NULL,
  LanguageName varchar(255) NOT NULL,
  ReleaseDate date NOT NULL,
  Age int NOT NULL,
  MovieDescription varchar(255) NOT NULL,
  ImageUrl varchar(255) NOT NULL,
  TrailerUrl varchar(255) NOT NULL,
  PRIMARY KEY (MovieId),
  CONSTRAINT MovieIdFK FOREIGN KEY (MovieId) REFERENCES List (ListId) ON UPDATE CASCADE
);

-- Create Serie table
CREATE TABLE Serie (
  SerieId int NOT NULL,
  SerieName varchar(255) NOT NULL,
  Season int NOT NULL,
  Episode int NOT NULL,
  Genre varchar(50) NOT NULL,
  RunTime int NOT NULL,
  LanguageName varchar(255) NOT NULL,
  ReleaseDate date NOT NULL,
  Age int NOT NULL,
  MovieDescription varchar(255) NOT NULL,
  ImageUrl varchar(255) NOT NULL,
  TrailerUrl varchar(255) NOT NULL,
  PRIMARY KEY (SerieId),
  CONSTRAINT SerieIdFK FOREIGN KEY (SerieId) REFERENCES List (ListId) ON UPDATE CASCADE
);

